---
title: "Figures for Thesis Group"
author: "Laura Naslund"
date: "February 21, 2019"
output:
  word_document: default
  html_document:
    df_print: paged
---

#Setup
###Note: I had an unexpectedly high spider Se value at a reference site (LF). This high measurement is inconsistent with all other data collected at this site during this study (Se concentration in water, sediment, larval invertebrates (Gerson et al. in prep) and emergent insects) and prior studies (Lindberg et al. 2011; Arnold et al. 2014, 2017). I had no reasonable way to determine whether this outlier was real data or contamination, so all analyses were run with and without this point
```{r setup, include=FALSE, warning=FALSE}
knitr::opts_chunk$set(echo = TRUE)

#packages
library(tidyverse)
library(magrittr)
library(vegan)
library(lme4)
library(lmerTest)
library(ggrepel)

require(lubridate)
require(lmerTest)

#load dfs
families <- read.csv("emergents_families.csv")
emergents <- read.csv("emergents_id_2018.csv")
emerg_17 <- read.csv("emergent_mass_2017.csv")%>% rename_all(tolower)
traps <- read.csv("trap_dates_2018.csv")%>% mutate(trap_period = as.numeric(mdy(strike_day)-mdy(install_day)))
sites <- read.csv("mtm_2019_site_description.csv") %>% mutate(mined=as.factor(mined), ponded=as.factor(ponded))
water <- read.csv("water_data_2018.csv") %>% dplyr::select(-Site_Type, -Site1, -Site2) %>% rename_all(tolower)
spid_17 <- read.csv("spider_se_2017.csv")
spid_18 <- read.csv("spider_se_2018.csv")
spider_lab_count <- read.csv("spider_messy_counts.csv")
spider_field_count <- read.csv("transect_score_2018.csv")
spider_mass_18 <- read.csv("spider_mass_2018.csv")
spider_mass_17 <- read.csv("spider_mass_2017.csv")
transect_2017 <- read.csv("transect_score_2017.csv")
transect_2018 <- read.csv("transect_score_2018.csv")
emerg_se <- read.csv("emerg_se.csv")
emerg_se_meta <- read.csv("emerg_se_metadata.csv")
```

```{r mutate dfs, include=FALSE, warning=FALSE}
#modify dfs
##df combining site information and water information
sites_water <- left_join(sites, water, by="site")

#combine emergent Se values with metadata
se_all <- emerg_se %>% full_join(emerg_se_meta, by="sample_id")

#break up Se emergents by taxa
caly <- se_all %>% filter(species=="Calyopterigidae ") %>% left_join(sites_water, by="site")
chiro <- se_all %>% filter(species=="Chironomidae spp.") %>% left_join(sites_water, by="site")
cf <- se_all %>% filter(species=="adult cranefly") %>% left_join(sites_water, by="site")
ei <- se_all %>% filter(species=="emergent insect") %>% left_join(sites_water, by="site")

##df for total emergent mass by site: select on aquatic emergents, sum by site, add site characteristics
emerg_tot_mass <- emergents %>% 
  left_join(families, by="family") %>% filter(aquatic=="yes") %>% 
  group_by(site) %>% summarize(mass_tot= sum(mass_g, na.rm=T)) %>% 
  left_join(traps, by="site") %>% mutate(emerg_mg_m2_day=((mass_tot*1000)/(0.36*3*trap_period)))%>%
  left_join(sites_water, by="site") %>% mutate(catchment_area_ha = catchment_area_m2 * 0.0001) 

#df with only aquatic insects
emergents <- left_join(emergents %>% dplyr::select(-order), families, by="family") 
emerg_aq <- emergents %>%filter(aquatic=="yes")%>% filter(is.na(mass_g)==FALSE) %>% droplevels()

#long df with mass and individual composites by family
emerg_aq_summ <- emerg_aq %>% group_by(site, family, ept) %>% summarize(mass_sum= sum(mass_g), indiv_sum= sum(indiv_num))

#wide df with individual sums by family by site
emerg_count <- emerg_aq_summ %>% dplyr::select(-ept, -mass_sum) %>% spread(family, indiv_sum) %>% replace(is.na(.), 0)

#join 2017 and 2018 spider Se data and make tidy
#append year to se column
spid_17_t <- spid_17 %>% mutate(se_ug_g_2017 = se_ug_g) %>% dplyr::select(site, rep, -se_ug_g, se_ug_g_2017) 
spid_18_t <- spid_18 %>% mutate(se_ug_g_2018= se_ug_g) %>% dplyr::select(site, rep, -se_ug_g, se_ug_g_2018)
##ps the regex means split on underscore preceding the digit
spider_se <- full_join(spid_17_t, spid_18_t, by=c("site", "rep")) %>%  gather(se_ug_g_2017, se_ug_g_2018, key="collected", value="se_ug_g") %>% separate(collected, into=c("label", "year"), sep = "_(?=[:digit:])") %>% dplyr::select(-label) %>% arrange(site)
#calculate average and stdev for sites with multiple reps and add site descriptions
spider_se <- left_join(spider_se %>% filter(is.na(se_ug_g)==FALSE) %>% group_by(site, year) %>% summarize(se_av = mean(se_ug_g), se_sd = sd(se_ug_g)), sites_water, by="site") %>% mutate(mined=as.factor(mined), ponded=as.factor(ponded))

#Sum lab spider counts, exclude HC which didn't have a lab count for some reason
spider_count <- inner_join(spider_lab_count %>% filter(site!="HC") %>% group_by(site) %>% summarize(lab_count=n()), spider_field_count %>% group_by(site) %>% summarize(field_count =sum(count)), by="site")

#calculate 2018 mass
spider_mass <- spider_count %>% left_join(spider_mass_18, by="site") %>% mutate(spider_prop_mass_18= (field_count/lab_count)*spider_tot_mass)

#calculate 2017 mass, join with 2018 mass and add site info
spider_mass <- full_join(spider_mass, spider_mass_17, by="site") %>% mutate(spider_mass_17 = tot_mass) %>% dplyr::select(-tot_mass)
spider_mass <- left_join(spider_mass, sites, by="site")

#combine emergents and spider df
spid_emerg <- left_join(spider_mass,emerg_tot_mass %>% select(site, emerg_mg_m2_day), by="site")
names(spid_emerg)
spid_emerg <- left_join(spid_emerg, water, by="site")

#combine 2017 and 2018 transect data
transect_2017_c <- transect_2017 %>% dplyr::select(site, count, density, segment_num, shrub, overhanging.branches, roots, large.leaf.cover, small.leaf.cover, grass, moss, sand.rocks.dead.leaves) %>% mutate(moss_sand_rocks_dead_leaves=moss + sand.rocks.dead.leaves, leafy_veg=large.leaf.cover+small.leaf.cover) %>% dplyr::select(-large.leaf.cover, -small.leaf.cover, -moss, -sand.rocks.dead.leaves)

transect_2018_c <- transect_2018 %>% dplyr::select(site, count, density, segment_num, shrub, overhanging.branches, roots, leafy_veg, grass, moss.sand.rocks.dead.leaves) 

transect_2017_c <- transect_2017_c[,c(1, 2, 3, 4, 5, 6, 7, 10, 8,9)]

names(transect_2017_c) <- c("site", "count", "density", "segment_num", "shrub", "overhanging_branches", "roots", "leafy_veg", "grass", "moss_sand_rocks_dead_leaves")
names(transect_2018_c) <- c("site", "count", "density", "segment_num", "shrub", "overhanging_branches", "roots", "leafy_veg", "grass", "moss_sand_rocks_dead_leaves")

all_transect <- dplyr::bind_rows(transect_2017_c %>% mutate(year="2017"), transect_2018_c %>% mutate(year="2018"))
all_transect <- left_join(all_transect %>% mutate(osadd=overhanging_branches+shrub), sites_water, by="site")
```

```{r plot theme set up}
figure_theme <- function(plot){
  plot +
  geom_point(aes(shape=ponded), size=4, stroke=1) +
  scale_shape_manual(breaks = c("0", "1"), 
                     values=c(21,24),
                     labels= c("unponded", "ponded")) +
  geom_point(aes(color=mined, fill=mined, shape=ponded), alpha=0.4, size=4) +
  scale_shape_manual(breaks = c("0", "1"), 
                     values=c(21,24),
                     labels= c("unponded", "ponded")) +
  scale_color_manual(breaks = c("0", "1"), 
                    values=c("darkblue", "goldenrod4"),
                     labels= c("unmined", "mined")) +
   scale_fill_manual(breaks = c("0", "1"), 
                    values=c("darkblue", "goldenrod4"),
                     labels= c("unmined", "mined")) +
  theme_bw()+
  theme(axis.title = element_text(size=20, color='black'), axis.text = element_text(hjust=0.95, vjust=0.2, size=14, color='black'), legend.text = element_text(size = 14), legend.title=element_blank())
}
```

```{r}
spid_plot <- ggplot(spider_se %>% filter(year=="2018"), aes(percent_mined, se_av)) +
  geom_rect(aes(xmin=0, xmax=100, ymin = mean_2018_u_LF-ci_2018_fact_LF, ymax =mean_2018_u_LF+ci_2018_fact_LF), fill="grey92") +
  geom_rect(aes(xmin=0, xmax=100, ymin = mean_2018_u-ci_2018_fact, ymax =mean_2018_u+ci_2018_fact), fill="grey80") +
  annotate("text", size=5, x = 7, y = 29, label = "LF") +
  geom_hline(yintercept=mean_2018_u+ci_2018_fact, linetype="dashed")+
  geom_hline(yintercept=mean_2018_u-ci_2018_fact, linetype="dashed") +
  geom_hline(yintercept=mean_2018_u) + 
  geom_vline(xintercept=2, linetype="longdash", color="red",size=1)+
  geom_smooth(data=(spider_se%>%filter(year=="2018", mined==1)), method="lm", color="black", se=FALSE) + 
  theme_bw() +
  labs(x="Mining Extent %", y="Spider Se ug/g") +
  ylim(0,30)

figure_theme(spid_plot)

```

#Spider Se Analysis
```{r spider se stats}
#spider se summary stats
spider_se %>% 
  group_by(year, mined) %>% 
  summarize(mean_spid_se=mean(se_av), sd_spid_se=sd(se_av), min_spid_se=min(se_av), max_spid_se=max(se_av))

#spider se model with LF
spider.se.cont <- lmer(se_av~percent_mined + (1|site) + (1|year), data=spider_se)
spider.se.mined_p <- summary(spider.se.cont)$coefficients[2,5]

#spider se model without LF
spider.se.cont.lf <- lmer(se_av~percent_mined + (1|site) + (1|year), data=spider_se %>% filter(site!="LF"))
spider.se.mined_p.lf <- summary(spider.se.cont.lf)$coefficients[2,5]
```

```{r emergent se stats}
#for the paper you need to decide if you are going to include MR7, and LF
range(ei$se_ug_g)
summary(lm(se_ug_g ~ percent_mined, data=ei))
```

```{r emergent composite se figure}
mean_emerg_se <- mean((spider_se %>% filter(year=="2018", mined==0))$se_av)
sd_emerg_se <- sd((spider_se %>% filter(year=="2018", mined==0))$se_av)
n_emerg_se <- nrow(spider_se %>% filter(year=="2018", mined==0))
z <- 1.96
ci_emerg_se <- z*(sd_emerg_se/(sqrt(n_emerg_se)))

emerg_comp_se <- ggplot(ei, aes(percent_mined, se_ug_g)) + 
  geom_rect(aes(xmin=0, xmax=100, ymin = mean_emerg_se-ci_emerg_se, ymax =mean_emerg_se+ci_emerg_se), fill="grey80") +
  geom_hline(yintercept=mean_emerg_se+ci_emerg_se, linetype="dashed")+
  geom_hline(yintercept=mean_emerg_se-ci_emerg_se, linetype="dashed") +
  geom_smooth(data=(ei%>%filter(mined==1)), method="lm", color="black", se=FALSE) + 
  geom_point(aes(shape=ponded), size=4, stroke=1) +
  scale_shape_manual(breaks = c("0", "1"), 
                     values=c(21,24),
                     labels= c("unponded", "ponded")) +
  geom_point(aes(color=mined, fill=mined, shape=ponded), alpha=0.4, size=4) +
  scale_shape_manual(breaks = c("0", "1"), 
                     values=c(21,24),
                     labels= c("unponded", "ponded")) +
  scale_color_manual(breaks = c("0", "1"), 
                    values=c("darkblue", "goldenrod4"),
                     labels= c("unmined", "mined")) +
   scale_fill_manual(breaks = c("0", "1"), 
                    values=c("darkblue", "goldenrod4"),
                     labels= c("unmined", "mined")) +
  theme_bw()+
  labs(x="Mining Extent %", y="Emergent Insect Se ug/g") +
  theme(axis.title = element_text(size=20, color='black'), axis.text = element_text(hjust=0.95, vjust=0.2, size=14, color='black'), legend.text = element_text(size = 14), legend.title=element_blank()) +
   geom_vline(xintercept=2, linetype="longdash", color="red",size=1)

#emerg_comp_se
```

```{r, fig.width=9, fig.height=5}
unmined_color <- rgb(153,153,209, max=255)
mined_color <- rgb(209, 195,161, max=255)
levels(se_all$species)

se_taxa <- ggplot(se_all %>% filter(site=="MR7"|site=="LF", species!="emergent insect"), aes(x=reorder(species, se_ug_g), se_ug_g, fill=as.factor(site))) +
  geom_bar(stat="identity", position="dodge") +
  scale_x_discrete(labels=c("adult cranefly" = "Tipulidae", "Calopterygidae" = "Calopterygidae",
                              "Chironomidae spp." = "Chironomidae")) +
  scale_fill_manual(breaks = c("LF", "MR7"), 
                    values=c(unmined_color, mined_color),
                    labels= c("LF (unmined)", "MR7 (mined)")) +
  theme_bw()+
  theme(axis.title = element_text(size=20, color='black'), axis.text = element_text(size=14, color='black'), legend.text = element_text(size = 14), legend.title=element_blank()) +
  labs(y="Se Concentration (ug/g)" , x="Taxon")

#se_taxa
```

```{r emergents descriptive stats}
#emergence mass range
range(emerg_tot_mass$emerg_mg_m2_day)
#number of emergents captured
sum(emerg_aq$indiv_num)
#total number of insects captured
(emergents %>% summarize(sum(indiv_num, na.rm=T)))[1,1]
```

```{r emergence mass regression}
emass.cont <- lm(log10(mass_tot)~ percent_mined+log10(catchment_area_m2), data=emerg_tot_mass)
summary(emass.cont)
emass.catch_p <- (summary(emass.cont))$coefficients[3,4]
emass.mined_p <- (summary(emass.cont))$coefficients[2,4]
```

```{r emergence mass figure 1: catchment area x emergence}
e1 <- ggplot(emerg_tot_mass, aes(catchment_area_ha, emerg_mg_m2_day)) + 
  stat_smooth(fill='grey85', color="black", method='lm')+ 
  scale_x_continuous(trans='log10') +
  scale_y_continuous(trans='log10') +
  geom_point(aes(fill=percent_mined),pch=21, size=4)+
  scale_fill_gradient(low='white', high='black',  name="Mining \nExtent %") + 
  labs(x = "Catchment Size (ha)", y=bquote("Emergent Insect Mass (mg/ " ~m^2~"/day)")) +
  theme_bw() +
  theme(axis.title = element_text(size=20, color='black'), axis.text = element_text(size=14, color='black'), legend.text = element_text(size = 14), legend.title=element_text(size=20))+
  annotate("text", size=7, x = 75, y = 100, label = paste("p=",round(emass.catch_p, digits=5)))

e1
```

```{r percent mined v emergence}
e2 <- ggplot(emerg_tot_mass, aes(percent_mined, emerg_mg_m2_day)) + 
  scale_y_continuous(trans='log10') +
  geom_point(aes(fill=catchment_area_ha),pch=21, size=4)+
  scale_fill_gradient(low='white', high='black',trans="log10", name="Catchment \nSize (ha)") + 
  labs(x = "Mining Extent %", y=bquote("Emergent Insect Mass (mg/ " ~m^2~"/day)")) +
  theme_bw() +
 theme(axis.title = element_text(size=20, color='black'), axis.text = element_text(size=14, color='black'), legend.text = element_text(size = 14), legend.title=element_text(size=20))+
  annotate("text", size=7, x = 0, y = 100, hjust=0, label = paste("p=",round(emass.mined_p, digits=5)))

# e2
```

```{r}
se_order <- c("LF", "Stanley", "LB", "HC", "MR7", "BF", "RB", "MR2", "SF", "SpringBranch", "eDNA_26")
se_order_rev <- c("eDNA_26", "SpringBranch", "SF", "MR2", "RB", "BF", "MR7", "HC", "LB", "Stanley", "LF")
emerg_tot_mass %>% filter(lcn_site_18==1) %>% select(site)

levels(emerg_tot_mass$site)
fct_reorder(as.factor(site), se_order)
emerg_tot_mass

class(emerg_tot_mass$se_order_rev)

ggplot(emerg_tot_mass%>% filter(site %in% se_order_rev), aes(x=fct_relevel(site, se_order_rev), mass_tot))+
  geom_bar(aes(fill=mine_id),stat="identity")+
  coord_flip() +
  labs(y="Emergents Mass (g)", x="") +
  theme_bw()

```

```{r emergent barplot}
emerg_fig <- emerg_aq %>% group_by(site, family, ept, order) %>% summarize(mass_sum= sum(mass_g), indiv_sum= sum(indiv_num)) %>% left_join(sites_water, by="site")

ephem_col <- rgb(254, 238, 154, max=255)
order_color <- c("grey90", "grey80", ephem_col, "grey68", "grey55", "grey40", "grey25", "black")

emerg_bar_order <- ggplot(emerg_fig, aes(x=fct_reorder(site,percent_mined), y=mass_sum, fill=order)) +
  geom_bar(stat="identity")+ 
  labs(x="Site", y="Emergent Insect Mass") + 
  scale_fill_manual(name= "Insect\nOrder", values = order_color) +
  scale_x_discrete(
breaks=c("eDNA_26", "RB", "SpringBranch", "LF", "MR2", "MR7", "MR14", "HC", "BF", "Stanley", "ST", "SF", "LB", "MB"),
labels=c("HF",  "RB", "SpringBranch", "LF", "MR2", "MR7", "MR14", "HC", "BF", "Stanley", "ST", "SF", "LB", "MB"))+
  theme_bw()+
  theme(axis.title = element_text(size=20, color='black'), axis.text = element_text(angle=45, hjust=0.95, vjust=0.2, size=14, color='black'), legend.text = element_text(size = 12), legend.title=element_text(size=16))+
  geom_vline(xintercept = 5.5, linetype="longdash", color="red",size=1) 

tiff('emerg_order_line.tiff', units="in", width=10, heigh=6.5, res=300)
emerg_bar_order
dev.off()

```

```{r emergent NMDS}
#merged count df with site and water df
emerg_count_env <- left_join(emerg_count, sites_water, by="site") %>% ungroup(site) %>% mutate(site= as.factor(site))

#environmental factors df
nmds_env <- emerg_count_env[c(1,40:69)]

#family count df
nmds_count <-emerg_count_env[c(2:39)]
nmds_sqr <- sqrt(nmds_count)
head(nmds_sqr)
#site vector
nmds_site <- emerg_count_env %>% pull(site)

#run NMDS
set.seed(620)
#NMDS <- metaMDS(nmds_count,k=4, trymax=500, distance = "bray")
NMDS_sqr <- metaMDS(nmds_sqr, k=4, trymax=500, distance= "bray")

#print summary
#NMDS
NMDS_sqr

#print summary 
stressplot(NMDS_sqr)

data.scores.sqr <- as.data.frame(scores(NMDS_sqr))
data.scores.sqr$site <- nmds_site
col_order <- c("site", "NMDS1","NMDS2","NMDS3","NMDS4")
data.scores.sqr <- data.scores.sqr[, col_order]

#add env data to data scores
metaord_sqr <- left_join(data.scores.sqr, nmds_env, by="site")

species.scores.sqr <- as.data.frame(scores(NMDS_sqr, "species"))
species.scores.sqr$species <- rownames(species.scores.sqr)
ef <- envfit(NMDS_sqr, nmds_env %>%dplyr::select(-ponded,-mine_id, -lcn_site_17, -lcn_site_18, -thg_ng_l_filt , -mn_conc_ug_l_filt , -s_conc_mg_l_filt), permu = 999, na.rm=TRUE)
ef

env.scores <- as.data.frame(scores(ef, display = "vectors")) #extracts relevant scores from envifit
env.scores <- cbind(env.scores, env.variables = rownames(env.scores))

env.scores.sig <- env.scores %>% filter(env.variables %in% c("percent_mined","sulfate_mg_l", "tn_mg_l","potassium_mg_l ", "magnesius_mg_l", "calcium_mg_l", "conductivity", "s_conc_mg_l_unfilt"))

env.scores.sig$vec.name <- c("", "", "tn (mg/l)", "", "", "", "")

adonis(formula = vegdist(nmds_sqr, method = "bray") ~ catchment_area_m2, data = nmds_env) 
adonis(formula = vegdist(nmds_sqr, method = "bray") ~ percent_mined, data = nmds_env) 
adonis(formula = vegdist(nmds_sqr, method = "bray") ~ mined, data = nmds_env) 
```

```{r NMDS plot, fig.width=14, fig.height=10}
require(grid)
#sqr
mult <- 1.5
nmds.plot <-ggplot() + 
  geom_text_repel(data=species.scores.sqr,aes(x=NMDS1,y=NMDS2,label=species),alpha=0.5) +  # add the species labels
  geom_point(data=metaord_sqr,aes(x=NMDS1,y=NMDS2,fill=percent_mined),pch=21, size=5) + # add the point markers
  #geom_text_repel(data=metaord_sqr,aes(x=NMDS1,y=NMDS2,label=site),size=6,vjust=0) +  # add the site labels
  coord_equal() +
  scale_fill_continuous(low="white", high="black", name="Mining \nExtent %")+
  geom_segment(data = env.scores.sig,
                    aes(x = 0, xend = mult*NMDS1, y = 0, yend = mult*NMDS2),
                    arrow = arrow(length = unit(0.25, "cm")), colour = "grey") + 
  geom_text(data = env.scores.sig,
                 aes(x = mult*NMDS1, y = mult*NMDS2, label=vec.name),
                 size = 7,
                 hjust = -0.5)+
  annotate("text", size=7, x = -0.75, y = 0.9, label="AlkMD")+
  theme_bw() +
  theme(axis.title = element_text(size=20, color='black'), axis.text = element_text(size=14, color='black'), legend.text = element_text(size = 14), legend.title=element_text(size=20))


#nmds.plot

```

```{r tn}
#t.test(tn_mg_l~mined, data=left_join(spider_mass, water, by= "site"))
#ggplot(left_join(spider_mass, water, by= "site"), aes(mined, tn_mg_l)) + geom_boxplot() + geom_point()

# summary(lm(log10(emerg_mg_m2_day)~tn_mg_l, data=emerg_tot_mass))
# ggplot(emerg_tot_mass, aes(tn_mg_l, emerg_mg_m2_day)) + scale_y_continuous(trans="log10")+ geom_point()
```

```{r diversity setup}
emerg_div <- as.data.frame(nmds_site)
names(emerg_div) <- "site"

ephemeroptera <- emerg_count %>% gather(key="family", value="count",2:ncol(emerg_count)) %>% 
  arrange(site) %>% 
  left_join(families, by="family") %>% filter(ephemeroptera==1) %>%
  spread(key="family", value="count")

ept <- emerg_count %>% gather(key="family", value="count",2:ncol(emerg_count))  %>% 
  arrange(site) %>% 
  left_join(families, by="family") %>% filter(ept==1) %>% 
  dplyr::select(site, family, count)%>%  spread(key="family", value="count")

emerg_div$ephemeroptera <-specnumber(ephemeroptera[5:ncol(ephemeroptera)])
emerg_div$ept <- specnumber(ept[5:ncol(ept)])
emerg_div$shannon <- diversity(nmds_count, index = "shannon")
emerg_div$simpson <- diversity(nmds_count, index = "simpson")
emerg_div$spec_num <- specnumber(nmds_count)


emerg_div <- left_join(emerg_div, sites_water, by="site")
emerg_div$catchment_area_ha <- emerg_div$catchment_area_m2 *0.0001
```

```{r diversity descriptive stats}
#number of aquatic families
families %>% filter(aquatic=="yes") %>% nrow()

#overall richness range
range(emerg_div$spec_num)
```

```{r overall richness stats}
#overall richness
erich.cont <- glm(spec_num ~ percent_mined+ log10(catchment_area_m2), data=emerg_div, family=poisson(link="log"))
summary(erich.cont)
erich.catch_p <- summary(erich.cont)$coefficients[3,4]
erich.mined_p <- summary(erich.cont)$coefficients[2,4]

erich.mined_p
```

```{r overall richness plots}
or.c <- ggplot(emerg_div, aes(catchment_area_ha, spec_num)) + 
  geom_smooth(method='lm', fill='gray85', color="black") +
  geom_point(aes(fill=percent_mined), pch=21, size=4)+ 
  scale_fill_continuous(low="white", high="black", name="Mining Extent %") +
  scale_x_continuous(trans="log10") +
  labs(x = "Catchment Size (ha)", y="Emergent Insect Richness") +
  theme_bw() +
  theme(axis.title = element_text(size=20, color='black'), axis.text = element_text(size=14, color='black'), legend.text = element_text(size = 14), legend.title=element_text(size=20))+
annotate("text", size=7, x = 40, y = 25, hjust=0, label = paste("p=",round(erich.catch_p, digits=5)))

or.m <- ggplot(emerg_div, aes(percent_mined, spec_num)) + 
  geom_point(aes(fill=catchment_area_ha), pch=21, size=4)+ 
  scale_fill_continuous(low="white", high="black", trans="log10", name="Catchment \nArea (ha)") +
  labs(x = "Mining Extent (%)", y="Emergent Insect Richness") +
  theme_bw() +
  theme(axis.title = element_text(size=20, color='black'), axis.text = element_text(size=14, color='black'), legend.text = element_text(size = 14), legend.title=element_text(size=20))+
annotate("text", size=7, x = 0, y = 25, hjust=0, label = paste("p=",round(erich.mined_p, digits=5))) 

or.c
or.m
```

```{r}
ggplot(emerg_div, aes(percent_mined, spec_num)) + 
  geom_point(aes(shape=ponded), size=4, stroke=1) +
  scale_shape_manual(breaks = c("0", "1"), 
                     values=c(21,24),
                     labels= c("unponded", "ponded")) +
  geom_point(aes(color=mined, fill=mined, shape=ponded), alpha=0.4, size=4) +
  scale_shape_manual(breaks = c("0", "1"), 
                     values=c(21,24),
                     labels= c("unponded", "ponded")) +
  scale_color_manual(breaks = c("0", "1"), 
                    values=c("darkblue", "goldenrod4"),
                     labels= c("unmined", "mined")) +
   scale_fill_manual(breaks = c("0", "1"), 
                    values=c("darkblue", "goldenrod4"),
                     labels= c("unmined", "mined")) +
  labs(x = "Mining Extent (%)", y="Emergent Insect Richness") +
  geom_vline(xintercept=2, linetype="longdash", color="red",size=1)+
  theme_bw() +
  theme(axis.title = element_text(size=20, color='black'), axis.text = element_text(size=14, color='black'), legend.text = element_text(size = 14), legend.title=element_text(size=20))+
annotate("text", size=7, x = 0, y = 25, hjust=0, label = paste("p=",round(erich.mined_p, digits=5)))

ggplot(emerg_div, aes(catchment_area_ha, spec_num)) + 
  geom_smooth(method='lm', fill='gray85', color="black") +
   geom_point(aes(shape=ponded), size=4, stroke=1) +
  scale_shape_manual(breaks = c("0", "1"), 
                     values=c(21,24),
                     labels= c("unponded", "ponded")) +
  geom_point(aes(color=mined, fill=mined, shape=ponded), alpha=0.4, size=4) +
  scale_shape_manual(breaks = c("0", "1"), 
                     values=c(21,24),
                     labels= c("unponded", "ponded")) +
  scale_color_manual(breaks = c("0", "1"), 
                    values=c("darkblue", "goldenrod4"),
                     labels= c("unmined", "mined")) +
   scale_fill_manual(breaks = c("0", "1"), 
                    values=c("darkblue", "goldenrod4"),
                     labels= c("unmined", "mined")) +
  scale_x_continuous(trans="log10") +
  labs(x = "Catchment Size (ha)", y="Emergent Insect Richness") +
  theme_bw() +
  theme(axis.title = element_text(size=20, color='black'), axis.text = element_text(size=14, color='black'), legend.text = element_text(size = 14), legend.title=element_text(size=20))+
annotate("text", size=7, x = 40, y = 25, hjust=0, label = paste("p=",round(erich.catch_p, digits=5)))

```

```{r ept stats}
eept.cont <- glm(ept ~ percent_mined+log10(catchment_area_m2), data=emerg_div, family=poisson(link="log"))
summary(eept.cont)

range(emerg_div$ept)
```

```{r density stats}
den.cont <-lmer(density ~ osadd + percent_mined + (1|site) + (1|year), data=all_transect)
summary(den.cont)
osadd.p <- summary(den.cont)$coefficients[2,5]
mined.den.p <- summary(den.cont)$coefficients[3,5]
```

```{r spider mass set up}
spid_emerg_gather <- spid_emerg%>%
  gather(spider_prop_mass_18,spider_mass_17, key="collected", value="spider_mass") %>% 
  separate(collected, into=c("label", "year"), sep = "_(?=[:digit:])") %>% 
  dplyr::select(-label) %>% 
  arrange(site)
```

```{r spider mass stats}
range(spid_emerg_gather$spider_mass, na.rm=T)

#including 2017
spid.emerg.mass.cont <- lmer(log10(spider_mass)~ percent_mined + emerg_mg_m2_day + (1|year), data=spid_emerg_gather)
summary(spid.emerg.mass.cont)
spid.mass.emerg_p <- summary(spid.emerg.mass.cont)$coefficients[3,5]
spid.mass.mined_p <- summary(spid.emerg.mass.cont)$coefficients[2,5]

#without 2017
spid.emerg.mass.cont.18 <- lm(log10(spider_mass)~ percent_mined + emerg_mg_m2_day, data=spid_emerg_gather %>% filter(lcn_site_18==1))
summary(spid.emerg.mass.cont.18)
```

```{r spider mass figures}
sm <- ggplot(spid_emerg_gather, aes(percent_mined, spider_mass)) +
  geom_point(aes(fill=emerg_mg_m2_day), pch=21, size=4) +
  scale_fill_continuous(low="white", high="black", name=paste("Emergent \nInsect \nMass", "\n(mg/", expression(m^2), "/day)")) + 
  labs(x="Mining Extent %", y="Spider Mass (g dw)") + 
   theme_bw() +
   theme(axis.title = element_text(size=20, color='black'), axis.text = element_text(size=14, color='black'), legend.text = element_text(size = 14), legend.title=element_text(size=18))+
  annotate("text", size=7, x = 0, y = 1, hjust=0, label = paste("p=",round(spid.mass.mined_p, digits=5)))

sem <- ggplot(spid_emerg_gather, aes(emerg_mg_m2_day, spider_mass)) +
  geom_smooth(method="lm", fill="gray85", color="black") +
  geom_point(aes(fill=percent_mined), pch=21, size=4) +
  scale_fill_continuous(low="white", high="black", name="Percent Mined %") + 
  labs(x=paste("Emergent Insect Mass", "(mg/", expression(m^2), "/day)"), y= "Spider Mass (g dw)") + 
   theme_bw() +
   theme(axis.title = element_text(size=20, color='black'), axis.text = element_text(size=14, color='black'), legend.text = element_text(size = 14), legend.title=element_text(size=18))+
  annotate("text", size=7, x = 0, y = 1, hjust=0, label = paste("p=",round(spid.mass.emerg_p, digits=5)))

sm
sem 
```

```{r}
ggplot(spid_emerg_gather, aes(percent_mined, spider_mass)) +
   geom_point(aes(shape=ponded), size=4, stroke=1) +
  scale_shape_manual(breaks = c("0", "1"), 
                     values=c(21,24),
                     labels= c("unponded", "ponded")) +
  geom_point(aes(color=mined, fill=mined, shape=ponded), alpha=0.4, size=4) +
  scale_shape_manual(breaks = c("0", "1"), 
                     values=c(21,24),
                     labels= c("unponded", "ponded")) +
  scale_color_manual(breaks = c("0", "1"), 
                    values=c("darkblue", "goldenrod4"),
                     labels= c("unmined", "mined")) +
   scale_fill_manual(breaks = c("0", "1"), 
                    values=c("darkblue", "goldenrod4"),
                     labels= c("unmined", "mined")) + 
  labs(x="Mining Extent %", y="Spider Mass (g dw)") + 
   theme_bw() +
   theme(axis.title = element_text(size=20, color='black'), axis.text = element_text(size=14, color='black'), legend.text = element_text(size = 14), legend.title=element_text(size=18))+
  annotate("text", size=7, x = 0, y = 1, hjust=0, label = paste("p=",round(spid.mass.mined_p, digits=5)))+
  geom_vline(xintercept=2, linetype="longdash", color="red",size=1)

ggplot(spid_emerg_gather, aes(emerg_mg_m2_day, spider_mass)) +
  geom_smooth(method="lm", fill="gray85", color="black") +
   geom_point(aes(shape=ponded), size=4, stroke=1) +
  scale_shape_manual(breaks = c("0", "1"), 
                     values=c(21,24),
                     labels= c("unponded", "ponded")) +
  geom_point(aes(color=mined, fill=mined, shape=ponded), alpha=0.4, size=4) +
  scale_shape_manual(breaks = c("0", "1"), 
                     values=c(21,24),
                     labels= c("unponded", "ponded")) +
  scale_color_manual(breaks = c("0", "1"), 
                    values=c("darkblue", "goldenrod4"),
                     labels= c("unmined", "mined")) +
   scale_fill_manual(breaks = c("0", "1"), 
                    values=c("darkblue", "goldenrod4"),
                     labels= c("unmined", "mined")) + 
  labs(x=paste("Emergent Insect Mass", "(mg/", expression(m^2), "/day)"), y= "Spider Mass (g dw)") + 
   theme_bw() +
   theme(axis.title = element_text(size=20, color='black'), axis.text = element_text(size=14, color='black'), legend.text = element_text(size = 14), legend.title=element_text(size=18))+
  annotate("text", size=7, x = 0, y = 1, hjust=0, label = paste("p=",round(spid.mass.emerg_p, digits=5)))

```



```{r pond stats}
# shapiro.test((spider_se %>% filter(year==2017, mined==1))$se_av)
# histogram((spider_se %>% filter(ponded==1))$se_av)
wilcox.test(se_ug_g~ponded, data=ei %>% filter(mined==1))

pond.stat <- wilcox.test(se_av~ponded, data=spider_se %>% filter(mined==1, year==2017)) 
pond_p <- pond.stat$p.value
```


