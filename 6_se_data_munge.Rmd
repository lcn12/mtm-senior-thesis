---
title: "R Notebook"
output: html_notebook
---
```{r setup}
library(magrittr)
library(tidyverse)
knitr::opts_knit$set(root.dir = "C:/Users/lnaslund/Documents/MTM2019/mtm-senior-thesis/data")
```

```{r water}
site_select <- c("eDNA_26", "RB", "SpringBranch", "LF", "MR2", "MR7", "MR14", "HC", "BF", "Stanley", "ST", "SF", "LB", "MB")

water_17 <- read.csv("water_data_2017.csv") %>% rename_all(tolower) %>% filter(site %in% site_select)%>% dplyr::select(site, se_conc_ug_g_unfilt)
water_18 <- read.csv("water_data_2018.csv")%>% rename_all(tolower)%>% filter(site %in% site_select)%>% dplyr::select(site, se_conc_ug_l_unfilt) 

#convert bdl values to min detection and make numeric
for(i in 1:nrow(water_18)){
  if(water_18$se_conc_ug_l_unfilt[i]=="<0.5"){
    water_18$se_water_unfilt[i] <- 0.5
  }
  else{
    water_18$se_water_unfilt[i] <- as.numeric(as.character(water_18$se_conc_ug_l_unfilt[i]))
  }
}

#combine 2017 and 2018 water
water_temp <- water_18 %>% dplyr::select(-se_conc_ug_l_unfilt) %>% full_join(water_17, by="site") 
names(water_temp) <- c("site", "water_2018", "water_2017")

#gather 2017 and 2018 water and convert to ppm
water_f <- water_temp %>% gather(water_2018, water_2017, key="env_year", value="se_conc") %>% mutate(se_conc=se_conc/1000)

```

```{r sediment}
sed_17 <- read.csv("sediment_se_2017.csv") %>% rename_all(tolower) %>% filter(site %in% site_select) %>% dplyr::select(site, sed_type, sediment_se_ug_g_17)%>% spread(key=sed_type, value=sediment_se_ug_g_17) 
sed_18 <- read.csv("sediment_se_2018.csv") %>% filter(site %in% site_select)%>% dplyr::select(site, sed_type, sediment_se_ug_g_18)%>% spread(key=sed_type, value=sediment_se_ug_g_18)

names(sed_17) <- c("site", "bulk_2017", "fine_2017")
names(sed_18)<- c("site", "bulk_2018", "fine_2018")

for(i in 1:nrow(sed_18)){
  if(sed_18$bulk_2018[i]=="<0.040"){
    sed_18$bulk_2018_num[i] <- 0.040
  }
  else{
    sed_18$bulk_2018_num[i] <- as.numeric(as.character(sed_18$bulk_2018[i]))
  }
    
}

sed_bulk <- sed_18 %>% dplyr::select(-bulk_2018) %>% mutate(bulk_2018=bulk_2018_num) %>% dplyr::select(-bulk_2018_num, -fine_2018) %>% full_join(sed_17, by="site")%>% dplyr::select(-fine_2017) %>% gather(bulk_2018, bulk_2017, key="env_year", value="se_conc") 

sed_fine <- sed_18 %>% full_join(sed_17, by="site")%>% dplyr::select(site, fine_2018, fine_2017) %>% gather(fine_2018, fine_2017, key="env_year", value="se_conc")

sed_f <- sed_bulk %>% mutate(env_year=as.factor(env_year), se_conc=as.numeric(se_conc))%>% bind_rows(sed_fine %>% mutate(env_year=as.factor(env_year), se_conc=as.numeric(se_conc)))
```

```{r biofilm}
bio_17 <- read.csv("biofilm_se_2017.csv") %>% filter(site %in% site_select) %>% dplyr::select(site, data_id, biofilm_se_ug_g_17) %>% group_by(site) %>% summarize(mean_bio_se_17=mean(biofilm_se_ug_g_17))

bio_18 <- read.csv("biofilm_se_2018.csv")%>% filter(site %in% site_select) %>% dplyr::select(site, data_id, biofilm_se_ug_g_18) %>% group_by(site) %>% summarize(mean_bio_se_18=mean(biofilm_se_ug_g_18))

#combine biofilm Se data
bio_f <-bio_17 %>% full_join(bio_18, by="site") %>% dplyr::rename(biofilm_2017=mean_bio_se_17, biofilm_2018=mean_bio_se_18) %>% gather(biofilm_2017, biofilm_2018, key="env_year", value="se_conc")
```

```{r emergent insects}
#create blank df to bind to 2018 emergent Se to keep plot format consistent
ei_17_blank <- ei %>% dplyr::select(site) %>% mutate(env_year="emergent.insect_2017", se_conc=NA)

ei_f <- ei %>% filter(site %in% site_select) %>% dplyr::select(site, se_ug_g) %>% mutate(env_year="emergent.insect_2018") %>% rename(se_conc=se_ug_g) %>% dplyr::select(site, env_year, se_conc) %>% bind_rows(ei_17_blank)

```

```{r spider se}
spid_f <- spider_se %>% filter(site %in% site_select) %>% mutate(env_year=paste("spider_", year, sep="")) %>% dplyr::select(site, env_year, se_av) %>% rename(se_conc=se_av)
```

```{r stack}
env_media_se <- water_f %>% bind_rows(sed_f, bio_f, ei_f, spid_f) %>% left_join(sites_water %>% dplyr::select(site, mined, ponded, mine_id, lcn_site_17, lcn_site_18, percent_mined, catchment_area_m2), by="site") %>% mutate(env_year_copy=env_year) %>% separate(env_year_copy, sep="_", into=c("env", "year"))

env_media_se_17 <- env_media_se %>% filter(year=="2017")
env_media_se_18 <- env_media_se %>% filter(year=="2018")
```

```{r plot}
env_media_17 <- ggplot(env_media_se_17 %>% filter(env!="fine"), aes(x=fct_relevel(env, "water", "bulk", "biofilm", "emergent.insect", "spider"), se_conc, fill=fct_relevel(mine_id, "U", "M", "P"))) + 
  geom_boxplot()+
  scale_x_discrete(labels=c("water" = "Water", "bulk" = "Sediment",
                              "biofilm" = "Biofilm", "emergent.insect"= "Emergent\nInsects", "spider"="Spiders")) +
  theme_bw()+
  labs(x="Environmental Medium", y="Se Concentration (ug/g)")+
  theme(axis.title = element_text(size=20, color='black'), 
        axis.text = element_text(angle=45, hjust=0.95, vjust=0.2, size=16, color='black'), 
        legend.text = element_text(size = 16), legend.title=element_blank())+ 
  scale_fill_manual(labels=c("U"="\nunmined\n", "M"= "mined,\nunponded\n", "P"="mined,\nponded\n"), values=c("grey70", "black", "gold1")) + scale_y_log10()

env_media_18 <- ggplot(env_media_se_18 %>% filter(env!="fine"), aes(x=fct_relevel(env, "water", "bulk", "biofilm", "emergent.insect", "spider"), se_conc, fill=fct_relevel(mine_id, "U", "M", "P"))) + 
  geom_boxplot()+
  scale_x_discrete(labels=c("water" = "Water", "bulk" = "Sediment",
                              "biofilm" = "Biofilm", "emergent.insect"= "Emergent\nInsects", "spider"="Spiders")) +
  theme_bw()+
  labs(x="Environmental Medium", y="Se Concentration (ug/g)")+
  theme(axis.title = element_text(size=20, color='black'), 
        axis.text = element_text(angle=45, hjust=0.95, vjust=0.2, size=16, color='black'), 
        legend.text = element_text(size = 16), legend.title=element_blank())+ 
  scale_fill_manual(labels=c("U"="\nunmined\n", "M"= "mined,\nunponded\n", "P"="mined,\nponded\n"), values=c("grey70", "black", "gold1")) + scale_y_log10()

tiff('env_media_18.tiff', units="in", width=10, heigh=6.5, res=300)
env_media_18
dev.off()

```