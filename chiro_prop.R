head(emerg_aq_summ)

chiro_mass <- emerg_aq_summ %>% 
  group_by(site) %>% 
  filter(family=="Chironomidae") %>% 
  dplyr::select(mass_sum) %>% ungroup()

tot_mass <- emerg_aq_summ %>% 
  group_by(site) %>% summarize(mass_add=sum(mass_sum)) %>% ungroup()

chiro_se <- chiro_mass %>% left_join(tot_mass, by="site") %>% 
  mutate(chiro_prop=100*(mass_sum/mass_add)) %>% 
  left_join(ei %>% dplyr::select(site, se_ug_g, percent_mined, mined), by="site")


chiro_se <- chiro_se %>% mutate(se_flux=se_ug_g*mass_add)

emerg_se_flux <- emerg_tot_mass %>% 
  dplyr::select(site, percent_mined, mined, ponded, catchment_area_m2, emerg_mg_m2_day) %>% 
  left_join(ei %>% dplyr::select(site, se_ug_g)) %>% mutate(se_flux=se_ug_g*emerg_mg_m2_day*0.001)

#this one
ggplot(emerg_se_flux %>% filter(ponded==0), aes(percent_mined, se_flux)) + geom_point() + geom_text_repel(aes(label=site))


summary(lm(se_flux~percent_mined, data=emerg_se_flux))
wilcox.test(se_flux~mined, data=emerg_se_flux)

ggplot(emerg_se_flux, aes(percent_mined, emerg_mg_m2_day)) + geom_point(aes(color=catchment_area_m2))


