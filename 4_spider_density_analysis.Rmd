---
title: "4_spider_density_analysis"
author: "Laura Naslund"
date: "May 14, 2019"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
library(tidyverse)
library(magrittr)
library(lme4)
library(lmerTest)
library("piecewiseSEM")

require(lmerTest)
```

```{r density stats}
den.cont <-lmer(density ~ osadd + percent_mined + (1|site) + (1|year), data=all_transect)
summary(den.cont)
osadd.p <- summary(den.cont)$coefficients[2,5]
mined.den.p <- summary(den.cont)$coefficients[3,5]
```

```{r density combining all}
all_transect_sum <- left_join(all_transect %>% group_by(site, year) %>% summarize(count_sum=sum(count), osadd_sum=sum(osadd)/7), sites_water, by="site") %>% mutate(all_density=count_sum/50)

#Spider Density Plot 2017
density_2017.plt <- ggplot(all_transect_sum %>% filter(year=="2017"), aes(percent_mined, all_density)) + 
  theme_bw() +
  labs(x="Mining Extent %", y="Spider Density (individuals/m)") +
  theme(axis.title = element_text(size=20, color='black'), axis.text = element_text(hjust=0.95, vjust=0.2, size=14, color='black'), legend.text = element_text(size = 14), legend.title=element_blank())+
  ylim(0,3)

density_2017.plt <- figure_theme(density_2017.plt)

#Spider Density Plot 2018
density_2018.plt <- ggplot(all_transect_sum %>% filter(year=="2018"), aes(percent_mined, all_density)) + 
  theme_bw() +
  labs(x="Mining Extent %", y="Spider Density (individuals/m)") +
  theme(axis.title = element_text(size=20, color='black'), axis.text = element_text(hjust=0.95, vjust=0.2, size=14, color='black'), legend.text = element_text(size = 14), legend.title=element_blank())+
  ylim(0,3)

density_2018.plt <- figure_theme(density_2018.plt)
```