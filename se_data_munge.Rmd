---
title: "R Notebook"
output: html_notebook
---

```{r water}
list.files()
library(magrittr)
library(tidyverse)

site_select <- c("eDNA_26", "RB", "SpringBranch", "LF", "MR2", "MR7", "MR14", "HC", "BF", "Stanley", "ST", "SF", "LB", "MB")

water_17 <- read.csv("water_data_2017.csv") %>% rename_all(tolower) %>% filter(site %in% site_select)%>% dplyr::select(site, se_conc_ug_g_unfilt)
water_18 <- read.csv("water_data_2018.csv")%>% rename_all(tolower)%>% filter(site %in% site_select)%>% dplyr::select(site, se_conc_ug_l_unfilt) 

#convert bdl values to min detection and make numeric
for(i in 1:nrow(water_18)){
  if(water_18$se_conc_ug_l_unfilt[i]=="<0.5"){
    water_18$se_water_unfilt[i] <- 0.5
  }
  else{
    water_18$se_water_unfilt[i] <- as.numeric(as.character(water_18$se_conc_ug_l_unfilt[i]))
  }
}

#combine 2017 and 2018 water
water_temp <- water_18 %>% dplyr::select(-se_conc_ug_l_unfilt) %>% left_join(water_17, by="site") 
names(water_temp) <- c("site", "water_2018", "water_2017")

#gather 2017 and 2018 water and convert to ppm
water_f <- water_temp %>% gather(water_2018, water_2017, key="env_year", value="se_conc") %>% mutate(se_conc=se_conc/1000)

```

```{r sediment}
sed_17 <- read.csv("sediment_se_2017.csv") %>% rename_all(tolower) %>% filter(site %in% site_select) %>% dplyr::select(site, sed_type, sediment_se_ug_g_17)%>% spread(key=sed_type, value=sediment_se_ug_g_17) 
sed_18 <- read.csv("sediment_se_2018.csv") %>% filter(site %in% site_select)%>% dplyr::select(site, sed_type, sediment_se_ug_g_18)%>% spread(key=sed_type, value=sediment_se_ug_g_18)

names(sed_17) <- c("site", "bulk_17", "fine_17")
names(sed_18)<- c("site", "bulk_18", "fine_18")

for(i in 1:nrow(sed_18)){
  if(sed_18$bulk_18[i]=="<0.040"){
    sed_18$bulk_18_num[i] <- 0.040
  }
  else{
    sed_18$bulk_18_num[i] <- as.numeric(as.character(sed_18$bulk_18[i]))
  }
    
}

sed_bulk <- sed_18 %>% dplyr::select(-bulk_18) %>% mutate(bulk_18=bulk_18_num) %>% dplyr::select(-bulk_18_num, -fine_18) %>% left_join(sed_17, by="site")%>% dplyr::select(-fine_17) %>% gather(bulk_18, bulk_17, key="env_year", value="se_conc") 

sed_fine <- sed_18 %>% left_join(sed_17, by="site")%>% dplyr::select(site, fine_18, fine_17) %>% gather(fine_18, fine_17, key="env_year", value="se_conc")

sed_f <- sed_bulk %>% mutate(env_year=as.factor(env_year), se_conc=as.numeric(se_conc))%>% bind_rows(sed_fine %>% mutate(env_year=as.factor(env_year), se_conc=as.numeric(se_conc)))
```

```{r biofilm}
bio_17 <- read.csv("biofilm_se_2017.csv") %>% filter(site %in% site_select) %>% dplyr::select(site, data_id, biofilm_se_ug_g_17) %>% group_by(site) %>% summarize(mean_bio_se_17=mean(biofilm_se_ug_g_17))

bio_18 <- read.csv("biofilm_se_2018.csv")%>% filter(site %in% site_select) %>% dplyr::select(site, data_id, biofilm_se_ug_g_18) %>% group_by(site) %>% summarize(mean_bio_se_18=mean(biofilm_se_ug_g_18))

bio_18
```